#!/usr/bin/env python
#this is a python code to send emails in an automated fashion for the monthly bill and provides a link to view the grafana graphs
#it takes the output of liban's bash script as the value to be printed in the email.
#AWS monetary values are given in USD, due to being a U.S company. This script allows for the real time conversion of these values, taking accepted daily rates, to any major currency the client wishes (that are available in the module)
#As A.L is a U.K company these were converted to GBP
#originally, any currency symbol could be obtained via the currencycode module. But this proved currently insurmountable in python 3 and could be left to the next sprint.

#importation of python modules
import smtplib
import email.utils
import socket
import subprocess
from subprocess import check_output
from forex_python.converter import CurrencyRates, CurrencyCodes
from decimal import *

encoding = 'utf-8' #as utf-8 is called upon several times in the code it is defined as encoding for ease

ex = CurrencyRates() #this gives the conversion rate between currencies to be selected
code = CurrencyCodes() #in the end this part was not necessary, it was what would enable the printing of the £ sign in the email

mon = check_output(['./monthlybill.sh']) #monthly bill for AWS instances

real = mon.decode(encoding) #as 'mon' defined above is in bytes it needs to be decoded to be printed as a string in the email

money = ex.convert('USD', 'GBP', Decimal(real)) #this converts the monthly bill from USD (the currency in which AWS quotes) to GBP, the decimal function is required to support floating point arithmetic

#this block of code is what actually sends the email. the monthly bill value is rounded to 2 decimal places for ease of use
fromaddr = 'ubuntu@'+socket.gethostname() #this is the private DNS name of the server
toaddrs  = 'magnus@automationlogic.com' #my email for development purposes, this can be altered to that of the client/user
msg = "\r\n".join([
  "From: ubuntu@"+socket.gethostname(),
  "To: magnus@automationlogic.com",
  "Subject: Your monthly bill",
  "The monthly bill for your AWS services in GBP is: "+str(round(money,2)),
  "To view the grafana graphs click on the link: http://grafana.grads.al-labs.co.uk./login"
  ])
server = smtplib.SMTP('localhost')
server.ehlo()
server.starttls()
server.sendmail(fromaddr, toaddrs, msg)
server.quit()
