#!/bin/bash

# Acquires dates in correct format.
#this bash script provides the value of the average weekly instances to be called upon in the email scripts
PATH=$PATH:/usr/local/bin #this is in place to enable the cron job to successfully trigger
monday=$(date --date="last Monday" +"%Y-%m-%d") # Start of week
today=$(date +"%Y-%m-%dT%H:%M:%SZ")

# Request data from cloudwatch over the last week and calculates the avg value
# for number of ec2 instances from all regions.

aws2 cloudwatch get-metric-statistics --metric-name ResourceCount --start-time $monday --end-time $today \
--period 3600 --namespace AWS/Usage --statistics Maximum --dimensions Name=Type,Value=Resource \
Name=Resource,Value=vCPU  Name=Service,Value=EC2 Name=Class,Value=Standard/OnDemand \
--region eu-west-1 |jq .Datapoints[].Maximum | awk -F : '{sum+=$1} END {print sum/NR}' | \
awk '{printf("%d\n",$1 + 0.5)}'
