#!/bin/bash
# The following code focuses on listing all the dates inbetween the start of the
# of the week and current day.
#it provides the monetary value of the monthly bill to be called upon in the email scripts

PATH=$PATH:/usr/local/bin #this is in place to enable the cron job to successfully trigger

monday=$(date +"%Y-%m-%dT00:00:00Z")
today=$(date +"%Y-%m-%dT%H:%M:%SZ")
# Calculates the monthly bill
aws2 cloudwatch get-metric-statistics --metric-name EstimatedCharges --start-time $monday \
--end-time $today --period 3600 --namespace AWS/Billing --statistics Maximum --dimensions \
 Name=Currency,Value=USD --region us-east-1 | jq .Datapoints[].Maximum | \
 awk 'BEGIN{a=   0}{if ($1>0+a) a=$1} END{print a}'
