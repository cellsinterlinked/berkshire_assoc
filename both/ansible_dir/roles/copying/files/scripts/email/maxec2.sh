#!/bin/bash

# The following code focuses on listing all the dates inbetween the start of the
# of the week and current day.
#it provides the value of the maximum weekly instance number to be called upon in the email scripts
PATH=$PATH:/usr/local/bin
monday=$(date --date="last Monday" +"%Y-%m-%d")
today=$(date +"%Y-%m-%dT%H:%M:%SZ")

# Calculates the maximum number of ec2 instances over the week
aws2 cloudwatch get-metric-statistics --metric-name ResourceCount --start-time $monday --end-time $today \
--period 3600 --namespace AWS/Usage --statistics Maximum --dimensions Name=Type,Value=Resource \
Name=Resource,Value=vCPU  Name=Service,Value=EC2 Name=Class,Value=Standard/OnDemand \
--region eu-west-1 |jq .Datapoints[].Maximum | awk 'BEGIN{a=   0}{if ($1>0+a) a=$1} END{print a}'
