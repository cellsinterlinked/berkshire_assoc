#!/usr/bin/env python

#this is a python code to send emails in an automated fashion for the weekly breakdown of AWS services. The email includes: the average weekly number of AWS instances, the maximum number of AWS instances recored during the week and the weekly bill for such services as well as a link to view the grafana graphs.
#it takes the outputs of liban's bash scripts as the values to be printed in the email.
#AWS monetary values are given in USD, due to being a U.S company. This script allows for the real time conversion of these values, taking accepted daily rates, to any major currency the client wishes (that are available in the module)
#As A.L is a U.K company these were converted to GBP
#originally, any currency symbol could be obtained via the currencycode module. But this proved currently insurmountable in python 3 and could be left to the next sprint.

#importation of python modules
import smtplib
import email.utils
import socket
import subprocess
from subprocess import check_output
from forex_python.converter import CurrencyRates, CurrencyCodes #these modules allow for real time currency conversion and the printing of any selected currency symbol
from decimal import *

encoding = 'utf-8' #as utf-8 is called upon several times in the code it is defined as encoding for ease
ex = CurrencyRates()
code = CurrencyCodes() #in the end this part was not necessary, it was what would enable the printing of the £ sign in the email

av = check_output(['./avgec2.sh']) #average weekly number of AWS instances
avreal = av.decode(encoding) #as 'av' defined above is in bytes it needs to be decoded to be printed as a string in the email

max = check_output(['./maxec2.sh']) #maximum number of AWS instances recored during the week
realmax = max.decode(encoding) #the same encoding is implemented for the max instance value

week = check_output(['./weeklybill.sh']) #weekly bill for AWS instances
realweek = week.decode(encoding) #the same encoding is implemented for the weeklybill value
money = ex.convert('USD', 'GBP', Decimal(realweek)) #this converts the weeklybill from USD (the currency in which AWS quotes) to GBP, the decimal function is required to support floating point arithmetic
#sign = code.get_symbol('GBP') #this allowed for gathering the correct currency symbol

#this block of code is what actually sends the email. the weekly bill value is rounded to 2 decimal places for ease of use
fromaddr = 'ubuntu@'+socket.gethostname()
toaddrs  = 'magnus@automationlogic.com'
msg = "\r\n".join([
  "From: ubuntu@"+socket.gethostname(),
  "To: magnus@automationlogic.com",
  "Subject: AWS breakdown",

  "The total average number of AWS instances this week was: "+str(avreal),
  "The maximum number of AWS instances this week was: "+str(realmax),
  "The weekly bill in GBP for your AWS services is: "+str(round(money,2)),
  "To view the grafana graphs click on the link: http://grafana.grads.al-labs.co.uk./login"
  ])
server = smtplib.SMTP('localhost')
server.ehlo()
server.starttls()
server.sendmail(fromaddr, toaddrs, msg)
server.quit()
