#!/bin/bash

# Takes Date from the python script dailybill.py
PATH=$PATH:/usr/local/bin #this is in place to enable the cron job to successfully trigger
monday=$1
today=$2

# Calculates the avg daily bill
aws2 cloudwatch get-metric-statistics --metric-name EstimatedCharges --start-time $monday \
--end-time $today --period 3600 --namespace AWS/Billing --statistics Maximum --dimensions \
 Name=Currency,Value=USD --region us-east-1 | jq .Datapoints[].Maximum | sort -hr | awk 'NR==1 {first = $0} END {print; print first}' | \
 awk '{print $1 - old; old = $1}' | awk 'END{print}'
