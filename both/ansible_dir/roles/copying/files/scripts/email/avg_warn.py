#!/usr/bin/env python
#code for average total weekly instance number warning email
#this script follows near identical to the all.py/monthly.py scripts except it just sends an email alerting the client/user if the average number of weekly instances is high

import smtplib #importation of python modules
import email.utils
import socket
import subprocess
from subprocess import check_output

encoding = 'utf-8'

av = check_output(['./avgec2.sh'])
avreal = av.decode(encoding)

fromaddr = 'ubuntu@'+socket.gethostname()
toaddrs  = 'magnus@automationlogic.com'
msg = "\r\n".join([
  "From: ubuntu@"+socket.gethostname(),
  "To: magnus@automationlogic.com",
  "Subject: Warning",

  "Warning, average number of instances this week exceeded the threshold. Number: "+str(avreal)
  ])

server = smtplib.SMTP('localhost')
server.ehlo()
server.starttls()
server.sendmail(fromaddr, toaddrs, msg)
server.quit()
