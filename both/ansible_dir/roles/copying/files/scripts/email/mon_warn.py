#!/usr/bin/env python
#code for monthly bill warning email
#this script follows near identical to the all.py/monthly.py scripts except it just sends an email alerting the client/user if the monthly bill is high

#importation of the required python modules
import smtplib
import email.utils
import socket
import subprocess
from subprocess import check_output
from forex_python.converter import CurrencyRates, CurrencyCodes
from decimal import *

encoding = 'utf-8'

#these two functions are required to convert currencies
ex = CurrencyRates()
code = CurrencyCodes()

mon_amount = check_output(['./monthlybill.sh'])
real = mon_amount.decode(encoding)
pounds = ex.convert('USD', 'GBP', Decimal(real))

#sign = code.get_symbol('GBP') #£ is a non-ascii character snd these are not supported in the email format


fromaddr = 'ubuntu@'+socket.gethostname()
toaddrs  = 'magnus@automationlogic.com'
msg = "\r\n".join([
  "From: ubuntu@"+socket.gethostname(),
  "To: magnus@automationlogic.com",
  "Subject: AWS breakdown",

  "Warning, monthly AWS bill this month in GBP is: "
  ])

server = smtplib.SMTP('localhost')
server.ehlo()
server.starttls()
server.sendmail(fromaddr, toaddrs, msg + str(round(pounds,2)))
server.quit()
