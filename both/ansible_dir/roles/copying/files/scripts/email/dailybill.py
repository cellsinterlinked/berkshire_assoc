#!/usr/bin/env python

#this is a python script which gives the daily bill for use of AWS instances
from datetime import date, timedelta
import datetime
from dateutil.relativedelta import relativedelta, MO
import os
import subprocess

# The following code focuses on listing all the dates inbetween the start of the
# of the week and current day.
now = datetime.datetime.now()
today = date.today()
last_monday = today + relativedelta(weekday=MO(-1))
sdate = date(last_monday.year,last_monday.month, last_monday.day)   # start date
edate = date(now.year, now.month, now.day)   # end date

delta = edate - sdate       # as timedelta

for i in range(delta.days + 1):
    day = sdate + timedelta(days=i)
    begin = str(day)+"T00:00:00Z"
    end = str(day)+"T23:59:59Z"
    sx = subprocess.call(['sh', 'dailyrange.sh', begin, end])
    # The code above works out the avg spent for each day of the week
