#!/bin/bash

#this is a bash script that works out the weekly bill by summing up the individual bill for each day.
python dailybill.py | awk -F : '{sum+=$1} END {print sum}'
