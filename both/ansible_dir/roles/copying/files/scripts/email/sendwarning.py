#!/usr/bin/env python

#this is a python code that runs the warning scripts (these send warning emails) if the thresholds are exceeded
#the warning emails just alert the client/user they do not, for example, activately terminate idle/excess instances
import os

#this is an if statement. if the average number of instances for the week exceeds or is equal to a value (30 in this case), the appropriate warning email is sent via the running of the script which sends it
av = check_output(['./avgec2.sh'])
if (str(av) >= 30):
    os.system('python avg_warn.py')
    pass

#as above, if the maximum number of instances for the week exceeds or is equal to a value (100 in this case), the appropriate warning email is sent via the running of the script which sends it
av = check_output(['./avgec2.sh'])
max = check_output(['./maxec2.sh'])
if (str(max) >= 100):
    os.system('python max_warn.py')
    pass

#as above, if the weekly bill exceeds or is equal to a value (250 USD in this case), the appropriate warning email is sent via the running of the script which sends it
week = check_output(['./weeklybill.sh'])
if (str(week) >= 250):
    os.system('python week_warn.py')
    pass

#as above, if the monthly bill exceeds or is equal to a value (1000 USD in this case), the appropriate warning email is sent via the running of the script which sends it
mon = check_output(['./monthlybill.sh'])
if (str(mon) >= 1000):
    os.system('python mon_warn.py')
    pass
