#!/usr/bin/env python

#this script follows near identical to the all.py/monthly.py scripts except it just sends an email alerting the client/user if the weekly bill is high
#importation of python modules
import smtplib
import email.utils
import socket
import subprocess
from subprocess import check_output
from forex_python.converter import CurrencyRates, CurrencyCodes
from decimal import *

encoding = 'utf-8'

ex = CurrencyRates()
code = CurrencyCodes()

week = check_output(['./weeklybill.sh'])
realweek = week.decode(encoding)

pounds = ex.convert('USD', 'GBP', Decimal(realweek))
sign = code.get_symbol('GBP')

fromaddr = 'ubuntu@'+socket.gethostname()
toaddrs  = 'magnus@automationlogic.com'
msg = "\r\n".join([
  "From: ubuntu@"+socket.gethostname(),
  "To: magnus@automationlogic.com",
  "Subject: AWS breakdown",

  "Warning, this week's aws bill in GBP is: "
  ])

server = smtplib.SMTP('localhost')
server.ehlo()
server.starttls()
server.sendmail(fromaddr, toaddrs, msg  +str(round(pounds,2)))
server.quit()
