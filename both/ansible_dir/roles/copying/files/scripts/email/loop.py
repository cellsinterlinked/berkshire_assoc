#!/usr/bin/env python

#this a python code which takes the outputs of Liban's bash scripts and dumps them into a json file (in json formating)
#this json file is required for the alexa querying portion of the project/product
import subprocess
from subprocess import check_output
import time
import json

encoding = 'utf-8'

mon = check_output(['./monthlybill.sh'])
realmon = mon.decode(encoding)

av = check_output(['./avgec2.sh'])
realav = av.decode(encoding)

max = check_output(['./maxec2.sh'])
realmax = max.decode(encoding)

week = check_output(['./weeklybill.sh'])
realweek = week.decode(encoding)

data = {
    'monthlybill': realmon,

    'average_number': realav,

    'max_number': realmax,

    'week': realweek
}


with open("alexa_file.json", "w") as write_file:
    json.dump(data, write_file)
