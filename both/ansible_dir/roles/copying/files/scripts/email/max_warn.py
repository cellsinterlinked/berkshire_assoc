#!/usr/bin/env python
#code for max instance number warning email
#this script follows near identical to the all.py/monthly.py scripts except it just sends an email alerting the client/user if the weekly maximum number of instances is high

#importation of required python modules
import smtplib
import email.utils
import socket
import subprocess
from subprocess import check_output

encoding = 'utf-8'

max = check_output(['./maxec2.sh'])
realmax = max.decode(encoding)

fromaddr = 'ubuntu@'+socket.gethostname()
toaddrs  = 'magnus@automationlogic.com'
msg = "\r\n".join([
  "From: ubuntu@"+socket.gethostname(),
  "To: magnus@automationlogic.com",
  "Subject: Warning",

  "Warning, maximum number of instances this week exceeded the threshold. Number:"+str(realmax)
  ])

server = smtplib.SMTP('localhost')
server.ehlo()
server.starttls()
server.sendmail(fromaddr, toaddrs, msg)
server.quit()
