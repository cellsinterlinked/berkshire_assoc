# README #
This README documents the steps necessary to get this monitoring application functioning.
### Summary ###
This is an application of collecting info from Cloudwatch and as such is a monitoring application. Grafana gathers this data and displays them on graphs. Moreover, emails are automatically sent to an engineer with such info (including links to Grafana) via cron jobs.
One single Ubuntu virtual machine is created via ansible and is provisioned with all the above roles.
As an add on, an Alexa skill was enabled to query the data e.g vocally asking Alexa for the monthly AWS bill associated with the user's account yields a vocal response from the device. What's more, warning emails are sent to the engineer if certain thresholds associated with the data are exceeded.
### How do I get set up? ###
First git clone this repository.
Alter the all file:
- /ansible_dir/environments/group_vars/all contains hard coded AWS regions, subnets, keys etc. These should be altered to those the engineer wishes
To create the Ubuntu machine:
- Chmod +x the create file (as well as all other bash/python scripts) and then run it
```bash
cd /berkshire_assoc/both/ansible_dir/ ; chmod +x create
```
- then
```bash
cd /copying/files/scripts/email/ ; chmod +x *
```
- This automates the VM's build and provisioning by executing the roles in the create.yml file
- Make sure the hosts file has nothing proceeding [VMS] before doing this
The emails:
- The cron jobs automate the sending of the emails. The frequency with which they do this can be easily manipulated in the /cron/tasks/main.yml file
- You can change the toaddrs in the python email scripts in /copying/files/scripts/email to your desired email in order to send said emails to your address
- Moreover, the thresholds in the sendwarning.py can be changed. For example, if a monthly bill for one's AWS service usage is deemed alarming at $500 the warning email (mon_warn.py) can be sent by altering the if statement in sendwarning.py to have a $500 threshold
### Explanation of the roles ###
# set_default
- This sets python3 as default in order to execute the email python scripts
# install_postfix
- This is required in order to send gmail emails, the Ubuntu machine will thus act as a gmail server.
* Other guidelines

# Alexa #
Alexa commands are made up of two parts. The invocation and utterance. The invocation is the skill name. For us that is cloud info. The utterance is the command that you want cloud info to do. We currently have two commands.

1. Alexa ask cloud info for the number of instances
2. Alexa ask cloud info for the bill
The first command returns the average number of ec2 instances over the week
period starting in Monday.
The second command returns the monthly bill in USD ($)

### How do I get set up? ###

```bash
cd /berkshire_assoc/both/alexa/lambda/ ;
```

Run the following commands to create the lambda function
```bash
cd /berkshire_assoc/both/alexa/lambda/
./createlambda
```
Lambda function has to be in the US-east region. Take a note of the lambda ARN.

1. Create an Alexa skill on Amazon development console. Make sure there are two intents. TestIntent for ec2 instances and BillIntent for the monthly bill.

2. Connect Alexa skill to Lambda by using your Lambda ARN code

3. Make sure the Berk-server VM is up and running.


# Grafana #

Once ansbile-playbook has run. Use the URL http://grafana.grads.al-labs.co.uk/ to access Grafana. Use the credentials below to log in as admin
Username: admin
password: mypassword

Only the admin user has the ability to change the dashboard. Other users can create accounts, but are only given the reader role and therefore are unable to make any changes Grafana.

### Grafana dashboard ###

To access the dashboards, click the home button found on the top left of the site. Then click on billinginfo7. Found under the general heading.
